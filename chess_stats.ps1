Clear-Host

$myUser = Read-Host "Your chess.com username"
$opponent = Read-Host "Opponent's username (leave blank for all opponents)"
if (!$opponent) { $opponent = ".*"}

$myArchives = Invoke-WebRequest -Uri https://api.chess.com/pub/player/$myUser/games/archives | ConvertFrom-Json

$games = @()
foreach ($archive in $myArchives.archives) {
    $games += Invoke-WebRequest -Uri $archive | select -ExpandProperty content | ConvertFrom-Json
}

$timeClasses = $games.games | select -ExpandProperty time_class -Unique | sort

$totalwhiteWins     = 0
$totalwhiteLosses   = 0
$totalwhiteDraws    = 0
$totalwhiteAbandons = 0
$totalblackWins     = 0
$totalblackLosses   = 0
$totalblackDraws    = 0
$totalblackAbandons = 0

foreach ($timeClass in $timeClasses) {

    Write-Host -ForegroundColor Green "`n`n===== $($timeClass.toupper()) ====="

    $results = $games.games | where {$_.time_class -eq $timeClass} | select white,black

    $whiteWins     = 0
    $whiteLosses   = 0
    $whiteDraws    = 0
    $whiteAbandons = 0
    $blackWins     = 0
    $blackLosses   = 0
    $blackDraws    = 0
    $blackAbandons = 0
    
    foreach ($row in $results) {
    
        if (($row | select -ExpandProperty white | select -ExpandProperty username) -eq $myUser `
            -and ($row | select -ExpandProperty black | select -ExpandProperty username) -match $opponent) {
    
            switch (($row | select -ExpandProperty white | select -ExpandProperty result)) {
    
                "win"        { $whiteWins     += 1; $totalwhiteWins     += 1 }
                "checkmated" { $whiteLosses   += 1; $totalwhiteLosses   += 1 }
                "resigned"   { $whiteLosses   += 1; $totalwhiteLosses   += 1 }
                "agreed"     { $whiteDraws    += 1; $totalwhiteDraws    += 1 }
                "abandoned"  { $whiteAbandons += 1; $totalwhiteAbandons += 1 }
                "timeout"    { $whiteLosses   += 1; $totalwhiteLosses   += 1 }
            }
    
        } 
        
        if (($row | select -ExpandProperty black | select -ExpandProperty username) -eq $myUser `
            -and ($row | select -ExpandProperty white | select -ExpandProperty username) -match $opponent) {
    
            switch (($row | select -ExpandProperty black | select -ExpandProperty result)) {
    
                "win"        { $blackWins     += 1; $totalblackWins     += 1 }
                "checkmated" { $blackLosses   += 1; $totalblackLosses   += 1 }
                "resigned"   { $blackLosses   += 1; $totalblackLosses   += 1 }
                "agreed"     { $blackDraws    += 1; $totalblackDraws    += 1 }
                "abandoned"  { $blackAbandons += 1; $totalblackAbandons += 1 }
            }
    
        }
    
    }
    
    Write-Host "`n$myUser playing as White...
    Wins:      $whiteWins
    Losses:    $whiteLosses
    Draws:     $whiteDraws
    Abandoned: $whiteAbandons"
    
    Write-Host "`n$myUser playing as Black...
    Wins:      $blackWins
    Losses:    $blackLosses
    Draws:     $blackDraws
    Abandoned: $blackAbandons"
    
    Write-Host "`n$myUser Totals...
    Wins:      $($blackWins + $whiteWins)
    Losses:    $($blackLosses + $whiteLosses)
    Draws:     $($blackDraws + $whiteDraws)
    Abandoned: $($blackAbandons + $whiteAbandons)"
    
    
}

Write-Host -ForegroundColor Green "`n`n`n========================"
Write-Host -ForegroundColor Green "===== GRAND TOTALS ====="
Write-Host -ForegroundColor Green "========================"

Write-Host "`n$myUser playing as White...
Wins:      $totalwhiteWins
Losses:    $totalwhiteLosses
Draws:     $totalwhiteDraws
Abandoned: $totalwhiteAbandons"

Write-Host "`n$myUser playing as Black...
Wins:      $totalblackWins
Losses:    $totalblackLosses
Draws:     $totalblackDraws
Abandoned: $totalblackAbandons"

Write-Host "`n$myUser Grand Totals...
Wins:      $($totalblackWins + $totalwhiteWins)
Losses:    $($totalblackLosses + $totalwhiteLosses)
Draws:     $($totalblackDraws + $totalwhiteDraws)
Abandoned: $($totalblackAbandons + $totalwhiteAbandons)"
